import pytest
import sys
from unittest.mock import patch, MagicMock

@pytest.fixture(scope='module')
def action_helper(mocker):
    from phishermon.action import Action
    mocker.patch('phishermon.action.MitigateFile')
    mocker.patch('phishermon.action.MitigateProcess')
    mocker.patch('phishermon.action.MitigateProcess')
    return Action

@pytest.fixture(scope='module')
def correlation_helper(mocker):
    from phishermon.action import CorrelationIndicator
    mocker.patch('phishermon.action.MitigateFile')
    mocker.patch('phishermon.action.MitigateProcess')
    mocker.patch('phishermon.action.MitigateProcess')
    return CorrelationIndicator

def test_action_properties(mocker):
    Action = action_helper(mocker)

    act = Action('test_name', [])
    assert(act.name == 'test_name')
    assert(act.correlations == [])

    # Name needs to be a string
    with pytest.raises(ValueError):
        Action(name=12345, correlations=[])

def test_action_correlations(mocker):
    Action = action_helper(mocker)
    CorrelationIndicator = correlation_helper(mocker)

    corrs = [{'name': 'test', 'field': 'example', 
              'to_extract': {'process': 'foo', 'pid': 'bar'}, 
              'type': 'process'}]

    act = Action('test_name', corrs)

    assert(isinstance(act.correlations[0], CorrelationIndicator))


def test_correlation_indicator(mocker):
    CorrelationIndicator = correlation_helper(mocker)

    conf = {'name': 'test', 'field': 'example', 
            'to_extract': {'process': 'foo', 'pid': 'bar'}, 
            'type': 'process'}

    corr = CorrelationIndicator(conf)

    assert(corr.name == 'test')
    assert(corr.field == 'example')
    assert(corr.event_type == 'process')
    assert(corr.extract == {'process': 'foo', 'pid': 'bar'})

def test_correlation_indicator_types(mocker):
    CorrelationIndicator = correlation_helper(mocker)

    conf = {'name': 'test', 'field': 'example', 
            'to_extract': {'process': 'foo', 'pid': 'bar'}, 
            'type': 'process'}

    corr = CorrelationIndicator(conf)

    # Must be a string
    with pytest.raises(ValueError):
        corr.name = 12345

    # Must be a string
    with pytest.raises(ValueError):
        corr.field = 12345

    # Must be a string or None
    with pytest.raises(ValueError):
        corr.event_type = 12345
    
    # Must be part of valid_types
    with pytest.raises(ValueError):
        corr.event_type= 'not_a_valid_type'
    
    # Event type needs to be defined
    with pytest.raises(ValueError):
        corr.event_type = None
        corr.extract = {'foo': 'bar'} 

    # Extract needs to have the proper fields
    with pytest.raises(ValueError):
        corr.event_type = 'process'
        corr.extract = {'path': 'tmp'} 

     # Extract needs to be a dictionary
    with pytest.raises(ValueError):
        corr.extract = ['process', 'pid']
   

def test_extract_data(mocker):
    CorrelationIndicator = correlation_helper(mocker)

    conf = {'name': 'test', 'field': 'example', 
            'to_extract': {'process': 'process', 'pid': 'pid'}, 
            'type': 'process'}

    corr = CorrelationIndicator(conf)

    event = {'process': 'tmp.exe', 'pid': '1234', 'parent_process': 'foo.exe'}

    corr.extract_data(event)

    assert(corr.extracted_data['process'] == 'tmp.exe')
    assert(corr.extracted_data['pid'] == '1234')




