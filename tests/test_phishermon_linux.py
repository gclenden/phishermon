import pytest
import sys
from unittest.mock import patch, MagicMock


@pytest.fixture(scope='module')
def phishermon_windows(mocker):

    mocker.patch('platform.system', MagicMock(return_value='Linux'))

    # Need to fake the default file locations
    mocker.patch('phishermon.phishermon.os.path.exists', return_value=True)
    from phishermon import Phishermon
    mocker.patch('phishermon.phishermon.Electus')
    mocker.patch('phishermon.phishermon.SysmonHelper')
    mocker.patch('phishermon.phishermon.Action')

    return Phishermon

def test_actions(mocker):
    Phishermon = phishermon_windows(mocker)

    mocker.patch.object(Phishermon, 'get_actions')

    # library_conf and job_conf are mandatory
    with pytest.raises(NotImplementedError):
        Phishermon(library_conf='library.json', job_conf='jobs.json', action_conf='actions.json')

def test_etw(mocker):
    Phishermon = phishermon_windows(mocker)

    mocker.patch.object(Phishermon, 'get_actions')

    # library_conf and job_conf are mandatory
    with pytest.raises(NotImplementedError):
        Phishermon(library_conf='library.json', job_conf='jobs.json', etw=True)