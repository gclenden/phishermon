import pytest
import sys
from unittest.mock import patch, MagicMock


@pytest.fixture(scope='module')
def sysmon_helper_windows(mocker):

    mocker.patch('platform.system', MagicMock(return_value='Linux'))
    from phishermon.sysmon_helper import SysmonHelper

    # Need to fake the default sysmon event file location
    mocker.patch('phishermon.sysmon_helper.os.path.exists', return_value=True)

    return SysmonHelper


def test_defaults(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)  
    
    # Raises a value error on non windows systems because the evtx file is None by default
    with pytest.raises(ValueError):
        SysmonHelper()

def test_etw(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)  
    
    # Raises a not implemented error because ETW is only available on windows
    with pytest.raises(NotImplementedError):
        SysmonHelper(etw=True, evtx=False)