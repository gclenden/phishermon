import pytest
import sys
from unittest.mock import patch, MagicMock


@pytest.fixture(scope='module')
def phishermon_windows(mocker):

    mocker.patch('platform.system', MagicMock(return_value='Windows'))

    # Need to fake the default file locations
    mocker.patch('phishermon.phishermon.os.path.exists', return_value=True)
    from phishermon import Phishermon
    mocker.patch('phishermon.phishermon.Electus')
    mocker.patch('phishermon.phishermon.SysmonHelper')
    mocker.patch('phishermon.phishermon.Action')

    return Phishermon

def test_defaults(mocker):
    Phishermon = phishermon_windows(mocker)

    # library_conf and job_conf are mandatory
    ph = Phishermon(library_conf='library.json', job_conf='jobs.json')

    assert(ph.library_conf == 'library.json')
    assert(ph.job_conf == 'jobs.json')
    assert(ph.action_conf == None)
    assert(ph.evtx_file == None)
    assert(ph.evtx == True)
    assert(ph.etw == False)
    assert(ph.output_file == None)
    assert(ph.silent == False)


def test_library_conf(mocker):
    Phishermon = phishermon_windows(mocker)

    # Throws exception when missing library_conf value
    with pytest.raises(ValueError):
        Phishermon(job_conf='jobs.json')


def test_job_conf(mocker):
    Phishermon = phishermon_windows(mocker)

    # Throws exception when missing job_conf
    with pytest.raises(ValueError):
        Phishermon(library_conf='library.json')


def test_action_conf(mocker):
    Phishermon = phishermon_windows(mocker)

    mocker.patch.object(Phishermon, 'get_actions')

    Phishermon(library_conf='library.json', job_conf='jobs.json', action_conf='actions.json', etw=True)

    # Actions can only be taken when etw is true
    with pytest.raises(ValueError):
        Phishermon(library_conf='library.json', job_conf='jobs.json', action_conf='actions.json', etw=False)


def test_properties(mocker):
    Phishermon = phishermon_windows(mocker)
    ph = Phishermon(library_conf='library.json', job_conf='jobs.json')

    with pytest.raises(ValueError):
        ph.etw = 'not_a_bool'

    with pytest.raises(ValueError):
        ph.evtx = 'not_a_bool'

    with pytest.raises(ValueError):
        ph.silent = 'foo'

    # Should be an empty list since there are no actions defined
    assert(ph.actions == [])

def test_no_datasource(mocker):
    Phishermon = phishermon_windows(mocker)

    with pytest.raises(ValueError):
        Phishermon(library_conf='library.json', job_conf='jobs.json', etw=False, evtx=False)
