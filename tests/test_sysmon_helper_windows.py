import pytest
import sys
from unittest.mock import patch, MagicMock


@pytest.fixture(scope='module')
def sysmon_helper_windows(mocker):

    mocked_imports = {'etw': MagicMock(), 'winreg': MagicMock(), 'psutil': MagicMock(), 'easygui': MagicMock()}
    
    with patch.dict('sys.modules', mocked_imports):
        from phishermon.sysmon_helper import SysmonHelper

    mocker.patch('platform.system', MagicMock(return_value='Windows'))
    mocker.patch.object(SysmonHelper, 'start_evtx_parser')
    mocker.patch.object(SysmonHelper, 'start_etw_parser')

    # Need to fake the default sysmon event file location
    mocker.patch('phishermon.sysmon_helper.os.path.exists', return_value=True)

    return SysmonHelper

def test_defaults(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)    
    sysmon = SysmonHelper()
    
    assert(sysmon.etw == False)
    assert(sysmon.evtx == True)
    assert(sysmon.evtx_file == r'C:\Windows\System32\winevt\Logs\Microsoft-Windows-Sysmon%4Operational.evtx')
    assert(sysmon.continuous == False)
    assert(sysmon.silent == True)
    assert(sysmon.outfile == None)
    assert(sysmon.append_to_outfile == True)
    assert(sysmon.compact == False)
    assert(sysmon.very_compact == False)
    assert(sysmon.pretty_print == False)
    assert(sysmon.tree == False)

def test_etw_windows(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)  
    sysmon = SysmonHelper(etw=True, evtx=True)
    
    assert(sysmon.etw == True)
    # Test that evtx goes first
    assert(sysmon.event_queue == sysmon.evtx_queue)

    with pytest.raises(ValueError):
        sysmon.etw = 'not_a_bool'

def test_properties(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)  
    sysmon = SysmonHelper(continuous=True, silent=False, append_to_outfile=False,
                          compact=True, very_compact=True, tree=True)

    # Test continuous
    assert(sysmon.continuous == True)
    with pytest.raises(ValueError):
        sysmon.continuous = 'foo'

    # Test silent
    assert(sysmon.silent == False)
    with pytest.raises(ValueError):
        sysmon.silent = 'foo'

    # Test append to outfile
    assert(sysmon.append_to_outfile == False)
    with pytest.raises(ValueError):
        sysmon.append_to_outfile = 'foo'

    # Test compact
    assert(sysmon.compact == True)
    with pytest.raises(ValueError):
        sysmon.compact = 'foo'

    # Test very compact
    assert(sysmon.very_compact == True)
    with pytest.raises(ValueError):
        sysmon.very_compact = 'foo'   

    # Test tree
    assert(sysmon.tree == True)
    with pytest.raises(ValueError):
        sysmon.tree = 'foo'   

def test_pretty_print(mocker):
    SysmonHelper = sysmon_helper_windows(mocker)  
    sysmon = SysmonHelper(pretty_print=True)

    assert(sysmon.pretty_print == True)

    with pytest.raises(ValueError):
        SysmonHelper(compact=True, pretty_print=True)
    
    with pytest.raises(ValueError):
        SysmonHelper(very_compact=True, pretty_print=True)


