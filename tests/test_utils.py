import pytest
from datetime import datetime
import xmltodict
from collections import OrderedDict

from phishermon import utils


xml_example = r"""<Event xmlns="http://schemas.microsoft.com/win/2004/08/events/event">
<System><Provider Name="Microsoft-Windows-Sysmon" Guid="{5770385f-c22a-43e0-bf4c-06f5698ffbd9}"></Provider>
<EventID Qualifiers="">10</EventID>
<Version>3</Version>
<Level>4</Level>
<Task>10</Task>
<Opcode>0</Opcode>
<Keywords>0x8000000000000000</Keywords>
<TimeCreated SystemTime="2017-10-20 18:08:53.180515"></TimeCreated>
<EventRecordID>275206</EventRecordID>
<Correlation ActivityID="" RelatedActivityID=""></Correlation>
<Execution ProcessID="1852" ThreadID="3068"></Execution>
<Channel>Microsoft-Windows-Sysmon/Operational</Channel>
<Computer>ComputerName</Computer>
<Security UserID="S-1-5-18"></Security>
</System>
<EventData><Data Name="UtcTime">2017-10-20 18:08:53.176</Data>
<Data Name="SourceProcessGUID">{835bea66-3347-59ea-0000-001082ce0300}</Data>
<Data Name="SourceProcessId">3488</Data>
<Data Name="SourceThreadId">3744</Data>
<Data Name="SourceImage">C:\WINDOWS\Explorer.EXE</Data>
<Data Name="TargetProcessGUID">{835bea66-3b78-59ea-0000-001076664a00}</Data>
<Data Name="TargetProcessId">784</Data>
<Data Name="TargetImage">C:\WINDOWS\system32\mmc.exe</Data>
<Data Name="GrantedAccess">0x00001000</Data>
</EventData>
</Event>"""

def test_flatten_dict():
    nested = {'lvl1':{'lvl2':{'lvl3': 'value'}}}
    assert utils.flatten_dict(nested) == {'lvl1.lvl2.lvl3': 'value'}

def test_triple_nested():
    nested = {'lvl1':{'lvl2a':{'lvl3': 'value'}, 'lvl2b': {'lvl3a': 'foo', 'lvl3b': 'bar'}}}
    flattened = utils.flatten_dict(nested)

    assert flattened['lvl1.lvl2a.lvl3'] == 'value'
    assert flattened['lvl1.lvl2b.lvl3a'] == 'foo'
    assert flattened['lvl1.lvl2b.lvl3b'] == 'bar'

def test_flatten_xml_event():
    flattened = utils.flatten_xml_event(xml_example)

    assert isinstance(flattened, OrderedDict)
    assert flattened['EventData.Data.SourceProcessId'] == '3488'

def test_join_key_value_pairs():
    xml_parsed = xmltodict.parse(xml_example)
    joined = utils.join_key_value_pairs(xml_parsed['Event']['EventData']['Data'])

    assert isinstance(joined, OrderedDict)
    assert joined['SourceProcessId'] == '3488'

def test_timestamp_to_epoch():
    ts = datetime.now()
    assert utils.timestamp_to_epoch(ts.strftime('%Y-%m-%d %H:%M:%S.%f')) == ts.timestamp()









