.. currentmodule:: phishermon


Overview
~~~~~~~~

Phishermon is a utility that will help analysts detect/analyze malware using host based behavioural signatures. 
It was initally developed as part of the malware project at `G33KW33K IV <https://g33kw33k.ca/en/index.html>`__.
Phishermon uses `electus <https://electus.readthedocs.io/en/latest/>`__ which allows the user to create a library of flexible 
behavioural signatures that can be reused in different contexts. Once the signatures have been defined they can be combined in different ways 
such as looking for a specific sequence of events within a given period of time.

Phishermon allows you to write these behavioural signatures and test them on Windows host based events (provided by `Sysmon <https://docs.microsoft.com/en-us/sysinternals/downloads/sysmon>`__).
These events (which include process, file and network) are accessed in two ways: realtime and replay. Phishermon is cross-platform and can replay the events
using the event log file output by Sysmon. Additionally, on Windows phishermon gets a realtime feed of these events using event tracing for Windows (ETW).
When a set of signatures hit on the realtime feed, phishermon will take an action (if specified). These actions include terminating a process, deleting a file, and deleting a registry value.

Installation
~~~~~~~~~~~~

Phishermon uses python 3 and will run on Windows and Linux. To install on Linux just run::

    pip install phishermon

To install on Windows you will need to install a python distribution (I recommend `Anaconda <https://conda.io/docs/user-guide/install/windows.html>`__).
After you have python configured run::
    
    pip install phishermon

To get the realtime feed of data you will also need to install Sysmon. Instructions to do so are `here <https://docs.microsoft.com/en-us/sysinternals/downloads/sysmon>`__.
You will also need to configure Sysmon to generate the proper events. I recommend starting with the `config <https://github.com/SwiftOnSecurity/sysmon-config>`__ created by `@SwiftOnSecurity <https://twitter.com/SwiftOnSecurity>`__.

Usage
~~~~~

Writing signatures
------------------

Phishermon uses `electus <https://electus.readthedocs.io/en/latest/>`_ as its signaturing engine. The specification for the library and jobs files can be found `here <https://electus.readthedocs.io/en/latest/>`__.
The library defines a signature and a job determines how it is used in combination with other signatures. For example

::

    {
        "word_launching_powershell": 
            {"datetime_field": "System.TimeCreated.SystemTime",
             "weight": 10,
             "indicators": [
                            {"field": "EventData.Data.Image", "value": ".*powershell.exe", "case_sensitive": false, "is_regex":true},
                            {"field": "EventData.Data.ParentImage", "value": ".*winword.exe", "case_sensitive": false, "is_regex":true}
                           ]

            },
        "powershell_making_network_connection": 
            {"datetime_field": "System.TimeCreated.SystemTime",
             "indicators": [
                            {"field": "System.EventID.text", "value": "3"},
                            {"field": "EventData.Data.Image", "value": ".*\\powershell.exe", "is_regex":true}
                           ]
            },
    }

This library has two signatures. The first looks for powershell.exe launched by Microsoft Word (winword.exe). The second looks for powershell making network connections.
We can define a job that looks for winword.exe launching powershell.exe then creating a network connection. For example

::

    {
    "winword_launching_powershell_with_network": 
        {"type": "sequence", 
         "features": ["word_launching_powershell", "powershell_making_network_connection"], 
         "sequence": ["word_launching_powershell", "powershell_making_network_connection"], 
         "time_window": 15
        }
    } 


Defining actions
----------------

Phishermon allows you to define an action to take if a job hits (Windows ETW feed only). These actions are killing a process, deleting a file, and deleting a registry value.
This can be dangerous so be careful! I recommend only doing this in a malware detonation environment.

Actions have a configuration file format that looks very similar to the library and job specifications. The following fields are required:

:name: Name of the job in the job configuration file.
:field: Field name to compare between hit objects. An action will only be taken if all the values of field in the hit objects match.
:to_extract: A dictionary of fields to extract to take action. The required fields for each type are {‘process’: [‘process’, ‘pid’], ‘file’: [‘path’], ‘registry’: [‘path’, ‘key’]}.
:type: The type of event to take action on. Valid event types are process, file, or registry.

If we wanted to kill the powershell process found using the configuration above we can define the following actions.json::

    {
        "winword_launching_powershell_with_network": [
            {"name": "powershell_making_network_connection",
             "field": "EventData.Data.ProcessId",
             "to_extract": {"Process": "EventData.Data.Image", "PID": "EventData.Data.ProcessId"},
             "type": "process"
            },
            {"name": "word_launching_powershell", "field": "EventData.Data.ProcessId"}
        ]
    }

This will kill the powershell process only if the ProcessId of the powershell process creating the network connection 
matches that of the process launched by winword.exe.


Putting it all together
-----------------------

After installing phishermon just run::

    phishermon --library library.json --jobs jobs.json --actions actions.json

To see all command line options run::

    phishermon -h

License
~~~~~~~

Phishermon is MIT licensed. Enjoy.

Contributing
~~~~~~~~~~~~

Contributions are more than welcome! Feel free to submit a pull request or create an issue and I will get to it as soon as I can.

Similar projects
~~~~~~~~~~~~~~~~

- `Sigma <https://github.com/Neo23x0/sigma>`_


