.. _api-ref:

Phishermon API reference
=========================

.. automodule:: phishermon
   :members:

Action
--------------------------

.. automodule:: phishermon.action

.. autoclass:: Action
  :members:
  :member-order: bysource

.. autoclass:: CorrelationIndicator
  :members:
  :member-order: bysource

Mitigations
--------------------------

.. automodule:: phishermon.mitigations

.. autoclass:: MitigateProcess
  :members:
  :member-order: bysource

.. autoclass:: MitigateFile
  :members:
  :member-order: bysource

.. autoclass:: MitigateRegistry
  :members:
  :member-order: bysource

Phishermon
--------------------------

.. automodule:: phishermon.phishermon

.. autoclass:: Phishermon
  :members:
  :member-order: bysource

Sysmon_helper
--------------------------

.. automodule:: phishermon.sysmon_helper

.. autoclass:: ProcessNode
  :members:
  :member-order: bysource

.. autoclass:: ProcessTree
  :members:
  :member-order: bysource

.. autoclass:: SysmonHelper
  :members:
  :member-order: bysource

Utils
--------------------------

.. automodule:: phishermon.utils
  :members: